from django.dispatch import Signal, receiver
from django.db.models.signals import post_save

from calls.models import CallRecord


new_completed_call_added = Signal(providing_args=[
    'subscriber', 'destination',
    'call_id', 'started_at',
    'ended_at', 'duration',
])


@receiver(post_save, sender=CallRecord)
def call_record_save_handler(sender, instance, **kwargs):
    dispatch_new_completed_call_added(instance)


def dispatch_new_completed_call_added(call_record):
    call_records_pair = CallRecord.objects.get_pair(call_id=call_record.call_id)
    start_call_record, end_call_record = call_records_pair
    if start_call_record and end_call_record:
        duration = call_record.get_duration(start_call_record.timestamp)
        new_completed_call_added.send(
            sender='calls',
            subscriber=start_call_record.source,
            destination=start_call_record.destination,
            call_id=start_call_record.call_id,
            started_at=start_call_record.timestamp,
            ended_at=end_call_record.timestamp,
            duration=duration)
