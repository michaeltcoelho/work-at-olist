import coreapi
from rest_framework import generics
from rest_framework.schemas import AutoSchema

from calls.models import CallRecord
from calls.serializers import CallRecordSerializer


class CallRecordCreateAPIView(generics.CreateAPIView):
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                'type',
                required=True,
                location='form',
                description='Type can be "start" or "end".'
            ),
            coreapi.Field(
                'call_id',
                required=True,
                location='form',
                description='Unique key pair identifier for a call record'
            ),
            coreapi.Field(
                'timestamp',
                required=True,
                location='form',
                description='Call record timestamp.'
            ),
            coreapi.Field(
                'source',
                required=False,
                location='form',
                description='Origin phone number.'
            ),
            coreapi.Field(
                'destination',
                required=False,
                location='form',
                description='Destination phone number.'
            ),
        ]
    )
    queryset = CallRecord.objects.all()
    serializer_class = CallRecordSerializer
