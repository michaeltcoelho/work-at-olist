from rest_framework import serializers

from common.serializers import PhoneField
from calls.models import CallRecord


class CallRecordSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='record_type')
    source = PhoneField(required=False)
    destination = PhoneField(required=False)

    class Meta:
        model = CallRecord
        fields = [
            'type', 'call_id', 'source',
            'destination', 'timestamp',
        ]

    def validate(self, data):
        record_type = data.get('record_type')
        source = data.get('source')
        destination = data.get('destination')
        call_id = data.get('call_id')

        if record_type == CallRecord.START:
            if CallRecord.start_calls.filter(call_id=call_id).exists():
                raise serializers.ValidationError(
                    f'Call {call_id} has already been started.')
            if not source or not destination:
                raise serializers.ValidationError(
                    'source and destination fields are required.')
            if source == destination:
                raise serializers.ValidationError(
                    'source and destination shoud not be equals.')
        else:
            if CallRecord.end_calls.filter(call_id=call_id).exists():
                raise serializers.ValidationError(
                    f'Call {call_id} has already been completed.')
        return data
