from django.conf.urls import url

from calls import views

urlpatterns = [
    url(r'calls$', views.CallRecordCreateAPIView.as_view(), name='calls'),
]
