from django.db import models


class StartCallRecordManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(record_type=CallRecord.START)


class EndCallRecordManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(record_type=CallRecord.END)


class CallRecordQuerySet(models.QuerySet):

    def get_pair(self, call_id):
        qs = self.filter(call_id=call_id)
        start_call_record = qs.filter(record_type=CallRecord.START).first()
        end_call_record = qs.filter(record_type=CallRecord.END).first()
        return start_call_record, end_call_record


class CallRecord(models.Model):

    START, END = 'start', 'end'
    TYPES = (
        (START, START),
        (END, END),
    )

    record_type = models.CharField('Type', max_length=5, choices=TYPES, default=START)
    call_id = models.IntegerField('Call ID')
    source = models.CharField('Source', max_length=11, blank=True, null=True)
    destination = models.CharField('Destination', max_length=11, blank=True, null=True)
    timestamp = models.DateTimeField('Ocurred at')

    objects = CallRecordQuerySet.as_manager()
    start_calls = StartCallRecordManager()
    end_calls = EndCallRecordManager()

    class Meta:
        verbose_name = 'Call record'
        verbose_name_plural = 'Call records'

    def __str__(self):
        return str(self.call_id)

    def get_duration(self, timestamp):
        if self.record_type == self.START:
            return timestamp - self.timestamp
        else:
            return self.timestamp - timestamp
