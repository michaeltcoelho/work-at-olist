from datetime import datetime

from factory.django import DjangoModelFactory

from calls.models import CallRecord


class CallRecordFactory(DjangoModelFactory):
    class Meta:
        model = CallRecord

    record_type = CallRecord.START
    call_id = 10
    source = '1699866865'
    destination = '1699866866'
    timestamp = datetime.utcnow()
