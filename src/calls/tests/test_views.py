from datetime import datetime
from unittest import mock

from rest_framework import status
from rest_framework.test import APITransactionTestCase
from rest_framework.reverse import reverse

from calls.tests.factories import CallRecordFactory, CallRecord


class CreateCallRecordViewTestCase(APITransactionTestCase):

    def get_calls_endpoint(self):
        return reverse('calls')

    def test_should_fields_be_required(self):
        endpoint = self.get_calls_endpoint()
        data = {
            'type': 'start',
        }
        response = self.client.post(endpoint, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertListEqual(
            list(response.json()),
            ['call_id', 'timestamp'])

    def test_should_source_and_destination_numbers_be_valid_numbers(self):
        endpoint = self.get_calls_endpoint()
        data = {
            'type': 'start',
            'call_id': '10',
            'source': '123123',
            'destination': '12313',
            'timestamp': datetime.utcnow().isoformat(),
        }
        response = self.client.post(endpoint, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        errors = response.json()
        self.assertEqual(
            errors['source'][0],
            'Invalid phone number.')
        self.assertEqual(
            errors['destination'][0],
            'Invalid phone number.')

    def test_should_create_start_call_record(self):
        endpoint = self.get_calls_endpoint()
        data = {
            'type': 'start',
            'call_id': '10',
            'source': '1698575788',
            'destination': '1698575785',
            'timestamp': datetime.utcnow().isoformat(),
        }
        response = self.client.post(endpoint, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_should_destination_field_be_required(self):
        endpoint = self.get_calls_endpoint()
        data = {
            'type': 'start',
            'call_id': '10',
            'source': '1698575788',
            'timestamp': datetime.utcnow().isoformat(),
        }
        response = self.client.post(endpoint, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        errors = response.json()['non_field_errors']
        self.assertEqual(errors[0], 'source and destination fields are required.')

    def test_should_numbers_be_not_equals(self):
        endpoint = self.get_calls_endpoint()
        data = {
            'type': 'start',
            'call_id': '10',
            'source': '1698575788',
            'destination': '1698575788',
            'timestamp': datetime.utcnow().isoformat(),
        }
        response = self.client.post(endpoint, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        errors = response.json()['non_field_errors']
        self.assertEqual(errors[0], 'source and destination shoud not be equals.')

    def test_should_create_end_call_record(self):
        endpoint = self.get_calls_endpoint()
        data = {
            'type': 'end',
            'call_id': 100,
            'timestamp': datetime.utcnow().isoformat(),
        }
        response = self.client.post(endpoint, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_should_create_end_call_before_than_start_call_record(self):
        with mock.patch('calls.signals.new_completed_call_added.send')\
                as mocked_new_completed_call_added:
            CallRecordFactory(record_type=CallRecord.END, call_id=10)
            self.assertFalse(mocked_new_completed_call_added.called)

        data = {
            'type': 'start',
            'call_id': 10,
            'source': '1698575788',
            'destination': '1698575785',
            'timestamp': datetime.utcnow().isoformat(),
        }
        with mock.patch('calls.signals.new_completed_call_added.send')\
                as mocked_new_completed_call_added:
            endpoint = self.get_calls_endpoint()
            response = self.client.post(endpoint, data=data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertTrue(mocked_new_completed_call_added.called)

    def test_should_not_start_call_twice_with_same_call_id(self):
        call_start_record = CallRecordFactory(call_id=100)
        data = {
            'type': 'start',
            'call_id': call_start_record.call_id,
            'source': '1698575788',
            'destination': '1698575785',
            'timestamp': datetime.utcnow().isoformat(),
        }
        response = self.client.post(
            self.get_calls_endpoint(),
            data=data,
            format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        payload = response.json()
        self.assertEqual(
            payload['non_field_errors'][0],
            f'Call {call_start_record.call_id} has already been started.')
