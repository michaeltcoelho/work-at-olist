from datetime import datetime, timedelta
from unittest import mock

from django.test import TestCase

from calls.models import CallRecord
from calls.tests.factories import CallRecordFactory


class CallRecordServiceTestCase(TestCase):

    @mock.patch('calls.signals.new_completed_call_added')
    def test_should_submit_call_to_billing(self, mocked_new_completed_call_added):
        start_call_record = CallRecordFactory(timestamp=datetime.utcnow())
        end_call_record_timestamp = start_call_record.timestamp + timedelta(minutes=3)
        end_call_record = CallRecordFactory(
            record_type=CallRecord.END,
            timestamp=end_call_record_timestamp)
        call_duration = end_call_record.get_duration(start_call_record.timestamp)
        call = mock.call(
            subscriber=start_call_record.source,
            destination=start_call_record.destination,
            call_id=end_call_record.call_id,
            started_at=start_call_record.timestamp,
            ended_at=end_call_record.timestamp,
            duration=call_duration,
            sender='calls',
        )
        self.assertTrue(mocked_new_completed_call_added.send.called)
        mocked_new_completed_call_added.send.assert_has_calls([call])
