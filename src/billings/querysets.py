from django.db import models


class PriceRuleQuerySet(models.QuerySet):

    def active(self):
        return self.latest('date_added')


class BillQuerySet(models.QuerySet):

    def get_invoice(self, subscriber, month, year):
        qs = self.prefetch_related('calls')\
            .get(subscriber=subscriber,
                 reference_month=month,
                 reference_year=year)
        return qs
