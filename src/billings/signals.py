from django.dispatch import receiver

from calls.signals import new_completed_call_added
from billings.services import process_billing_for_completed_call


@receiver(new_completed_call_added)
def completed_call_added_event_handler(sender, **kwargs):
    subscriber = kwargs['subscriber']
    call_destination = kwargs['destination']
    call_id = kwargs['call_id']
    call_ended_at = kwargs['ended_at']
    call_started_at = kwargs['started_at']
    call_duration = kwargs['duration']
    process_billing_for_completed_call(
        subscriber, call_destination,
        call_id, call_started_at,
        call_ended_at, call_duration
    )
