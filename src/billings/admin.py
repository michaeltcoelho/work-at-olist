from django.contrib import admin

from billings.models import PriceRule


@admin.register(PriceRule)
class PriceRuleAdmin(admin.ModelAdmin):
    pass
