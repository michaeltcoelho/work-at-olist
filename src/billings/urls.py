from django.conf.urls import url

from billings import views


urlpatterns = [
    url(r'billings/(?P<subscriber>\d{10,11})$',
        views.BillingAPIView.as_view(),
        name='billing'),
]
