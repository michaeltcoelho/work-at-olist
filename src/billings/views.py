from django.http import Http404

import coreapi
from rest_framework.response import Response
from rest_framework.schemas import AutoSchema
from rest_framework.views import APIView

from billings.models import Bill
from billings.serializers import (
    InvoiceRequestParamsSerializer,
    InvoiceSerializer,
)


class BillingAPIView(APIView):
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                'period',
                required=False,
                location='query',
                description='A period to be looked up for.'
            ),
        ]
    )

    def get(self, request, subscriber, format=None):
        serializer = InvoiceRequestParamsSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        period = serializer.validated_data['period']
        try:
            bill = Bill.objects.get_invoice(
                subscriber=subscriber,
                month=period.month,
                year=period.year)
        except Bill.DoesNotExist as err:
            raise Http404(str(err))
        response_serializer = InvoiceSerializer(bill)
        return Response(data=response_serializer.data)
