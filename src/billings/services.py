from billings.models import Bill, CallBalance, PriceRule


def process_billing_for_completed_call(
        subscriber, call_destination, call_id,
        call_started_at, call_ended_at, call_duration):
    """Process billing for completed calls.

    It will calculate the price of the call based on call's start
    and end timestamps using the lastest added PrirceRule.
    """
    current_bill, _ = Bill.objects.get_or_create(
        subscriber=subscriber,
        reference_month=call_started_at.month,
        reference_year=call_started_at.year,
    )

    price_rule = PriceRule.objects.active()
    call_price = price_rule.get_price(call_started_at, call_ended_at)

    CallBalance.objects.create(
        bill=current_bill,
        destination=call_destination,
        call_id=call_id,
        started_at=call_started_at,
        duration=call_duration,
        price=call_price,
    )
