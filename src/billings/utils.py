from datetime import datetime, timedelta


def get_timedelta_from_time(time_at):
    return timedelta(
        hours=time_at.hour,
        minutes=time_at.minute,
        seconds=time_at.second
    )


def time_in_range(date, start, end, time_at):
    start = datetime.combine(date, start)
    end = datetime.combine(date, end)
    time_at = datetime.combine(date, time_at)
    if end <= start:
        end += timedelta(1)
    if time_at <= start:
        time_at += timedelta(1)
    return start <= time_at <= end
