from datetime import date, timedelta

from rest_framework import serializers

from common.serializers import PeriodField, Period
from billings.models import Bill, CallBalance


def get_last_closed_period():
    past_month = date.today().replace(day=1) - timedelta(days=1)
    period = Period(month=past_month.month, year=past_month.year)
    return period


class InvoiceRequestParamsSerializer(serializers.Serializer):
    period = PeriodField(required=False, default=get_last_closed_period)


class InvoiceCallBalanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = CallBalance
        fields = ('call_id', 'started_at', 'duration', 'price')

    def to_representation(self, call_balance):
        return {
            'call_id': call_balance.call_id,
            'destination': call_balance.destination,
            'start_date': call_balance.started_at.date(),
            'start_time': call_balance.started_at.time(),
            'duration': call_balance.call_duration,
            'price': call_balance.price,
        }


class InvoiceSerializer(serializers.ModelSerializer):
    calls = InvoiceCallBalanceSerializer(many=True, read_only=True)

    class Meta:
        model = Bill
        fields = ('subscriber', 'reference_month', 'reference_year', 'calls')
