from decimal import Decimal
from datetime import datetime, time

from django.test import TestCase

from billings.tests.factories import PriceRuleFactory


class PriceRuleTestCase(TestCase):

    def test_should_validate_reduced_and_standard_tariff_times_calculation(self):
        price_rule = PriceRuleFactory()
        parametrize = (
            (datetime(2019, 1, 1, 21, 30, 0), datetime(2019, 1, 1, 23, 0, 0), 60, 30),
            (datetime(2019, 1, 1, 5, 0, 0), datetime(2019, 1, 1, 8, 0, 0), 60, 120),
            (datetime(2019, 1, 1, 4, 0, 0), datetime(2019, 1, 1, 6, 0, 0), 120, 0),
        )
        for param in parametrize:
            call_started_at, call_ended_at,\
                reduced_tariff_minutes_to_match, standard_tariff_minutes_to_match = param
            tariff_times = price_rule.get_reduced_and_standard_tariff_times(
                call_started_at,
                call_ended_at)
            reduced_tariff_minues, standard_tariff_minutes = tariff_times
            self.assertEqual(reduced_tariff_minues, reduced_tariff_minutes_to_match)
            self.assertEqual(standard_tariff_minutes, standard_tariff_minutes_to_match)

    def test_should_tariff_price_be_equal_to_standing_price(self):
        price_rule = PriceRuleFactory()
        reduced_tariff_minutes, standard_tariff_minutes = 0, 0
        tariff_price = price_rule.get_tariff_price(
            reduced_tariff_minutes,
            standard_tariff_minutes)
        self.assertEqual(tariff_price, price_rule.standing_price)

    def test_should_get_tariff_price_based_on_standard_tariff(self):
        price_rule = PriceRuleFactory()
        reduced_tariff_minutes, standard_tariff_minutes = 0, 120
        tariff_price = price_rule.get_tariff_price(
            reduced_tariff_minutes,
            standard_tariff_minutes)
        desired_tariff_price = price_rule.standing_price
        desired_tariff_price += Decimal('0.09') * standard_tariff_minutes
        self.assertEqual(tariff_price, desired_tariff_price)

    def test_should_validate_call_time_in_free_tariff_time_range(self):
        price_rule = PriceRuleFactory(
            reduced_tariff_start_time=time(22),
            reduced_tariff_end_time=time(6))
        parametrize = (
            (datetime(2019, 1, 1, 0, 0, 0), True),
            (datetime(2019, 1, 1, 3, 30, 0), True),
            (datetime(2019, 1, 1, 21, 30, 0), False),
            (datetime(2019, 1, 1, 13, 0, 0), False),
            (datetime(2019, 1, 1, 6, 10, 0), False),
        )
        for param in parametrize:
            call_time_at, is_in_range = param
            self.assertEqual(
                price_rule.is_in_reduced_tariff_time_range(call_time_at),
                is_in_range)

    def test_should_get_price(self):
        price_rule = PriceRuleFactory(
            reduced_tariff_start_time=time(22),
            reduced_tariff_end_time=time(6))
        call_start_at = datetime(2017, 12, 12, 4, 57, 13)
        call_end_at = datetime(2017, 12, 12, 6, 10, 56)
        price = price_rule.get_price(call_start_at, call_end_at)
        self.assertEqual(price, Decimal('1.26'))
