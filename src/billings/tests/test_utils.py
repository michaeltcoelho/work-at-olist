from datetime import date, time

from django.test import TestCase

from billings.utils import time_in_range


class TimeUtilsTestCase(TestCase):

    def test_should_validate_time_in_range(self):
        today = date.today()
        time_range_start_at = time(22)
        time_range_end_at = time(6)
        parametrize = (
            (today, time_range_start_at, time_range_end_at, time(21, 30), False),
            (today, time_range_start_at, time_range_end_at, time(6, 30), False),
            (today, time_range_start_at, time_range_end_at, time(14), False),
            (today, time_range_start_at, time_range_end_at, time(22, 1), True),
            (today, time_range_start_at, time_range_end_at, time(5, 59), True),
            (today, time_range_start_at, time_range_end_at, time(3, 52), True),
        )
        for param in parametrize:
            range_date, start_at, end_at, time_to_match, is_in_range = param
            evaluted_result = time_in_range(range_date, start_at, end_at, time_to_match)
            self.assertEqual(evaluted_result, is_in_range)
