from datetime import datetime, timedelta
from decimal import Decimal

from django.test import TestCase

from billings.models import CallBalance
from billings.services import process_billing_for_completed_call
from billings.tests.factories import PriceRuleFactory, BillFactory


def get_call_duration(call_started_at, call_ended_at):
    return call_ended_at - call_started_at


class ProcessBillingForCompletedCallTestCase(TestCase):

    def test_should_create_call_history(self):
        price_rule = PriceRuleFactory(
            standard_tariff_price=Decimal('0.09'),
            standing_price=Decimal('0.36'))
        call_started_at = datetime.utcnow()
        call_ended_at = call_started_at + timedelta(minutes=5)
        process_billing_for_completed_call(
            subscriber='16997864455',
            call_destination='16997864457',
            call_id=10,
            call_started_at=call_started_at,
            call_ended_at=call_ended_at,
            call_duration=get_call_duration(call_started_at, call_ended_at))
        call_history = CallBalance.objects.get(call_id=10)
        call_price = price_rule.get_price(call_started_at, call_ended_at)
        self.assertEqual(call_history.price, call_price)

    def test_should_process_billing_on_an_existent_bill(self):
        PriceRuleFactory()
        bill = BillFactory(subscriber='16998476677')
        call_started_at = datetime.utcnow()
        call_ended_at = call_started_at + timedelta(minutes=5)
        process_billing_for_completed_call(
            subscriber=bill.subscriber,
            call_destination='16997864458',
            call_id=10,
            call_started_at=call_started_at,
            call_ended_at=call_ended_at,
            call_duration=get_call_duration(call_started_at, call_ended_at))
        call_history = CallBalance.objects.get(call_id=10)
        self.assertEqual(call_history.bill, bill)

    def test_should_process_billing_for_a_reduced_tariff_call(self):
        price_rule = PriceRuleFactory(
            reduced_tariff_price=Decimal(0),
            standing_price=Decimal('0.36'))
        call_started_at = datetime.utcnow().replace(hour=1, minute=30, second=0)
        call_ended_at = call_started_at + timedelta(minutes=30)
        process_billing_for_completed_call(
            subscriber='16997864455',
            call_destination='16997864458',
            call_id=10,
            call_started_at=call_started_at,
            call_ended_at=call_ended_at,
            call_duration=get_call_duration(call_started_at, call_ended_at))
        call_history = CallBalance.objects.get(call_id=10)
        self.assertEqual(call_history.price, price_rule.standing_price)

    def test_should_process_billing_for_a_standard_tariff_call(self):
        price_rule = PriceRuleFactory()
        call_started_at = datetime.utcnow().replace(hour=13, minute=0)
        call_ended_at = call_started_at + timedelta(minutes=150)
        process_billing_for_completed_call(
            subscriber='16997864455',
            call_destination='16997864458',
            call_id=10,
            call_started_at=call_started_at,
            call_ended_at=call_ended_at,
            call_duration=get_call_duration(call_started_at, call_ended_at))
        call_history = CallBalance.objects.get(call_id=10)
        call_duration_in_minutes = get_call_duration(
            call_started_at,
            call_ended_at).seconds // 60
        call_price = price_rule.get_tariff_price(
            price_rule.standard_tariff_price,
            call_duration_in_minutes)
        self.assertEqual(call_history.price, call_price)
