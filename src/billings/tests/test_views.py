from datetime import datetime

from rest_framework import status
from rest_framework.test import APITransactionTestCase
from rest_framework.reverse import reverse

from billings.tests.factories import BillFactory, CallBalanceFactory


class BillingViewTestCase(APITransactionTestCase):

    def get_endpoint(self, phone_number):
        return reverse('billing', args=[phone_number])

    def test_should_not_get_malformed_period(self):
        endpoint = self.get_endpoint('16997935436')
        params = {
            'period': '12332',
        }
        response = self.client.get(endpoint, data=params, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        payload = response.json()
        self.assertIn('period', payload)
        self.assertEqual(
            payload['period'][0],
            'Malformed period. It should be in month/year.')

    def test_should_not_get_invalid_period(self):
        endpoint = self.get_endpoint('16997935436')
        params = {
            'period': '13/2019',
        }
        response = self.client.get(endpoint, data=params, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        payload = response.json()
        self.assertIn('period', payload)
        self.assertEqual(
            payload['period'][0],
            'Invalid period.')

    def test_should_get_last_closed_period(self):
        bill = BillFactory(reference_month=12, reference_year=2018)
        CallBalanceFactory(
            bill=bill,
            started_at=datetime(2018, 12, 2, 14, 0, 0))
        endpoint = self.get_endpoint(bill.subscriber)
        response = self.client.get(endpoint, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        payload = response.json()
        self.assertEqual(payload['subscriber'], bill.subscriber)
        self.assertEqual(payload['reference_month'], bill.reference_month)
        self.assertEqual(payload['reference_year'], bill.reference_year)
        self.assertEqual(len(payload['calls']), 1)

    def test_should_get_by_period(self):
        bill = BillFactory(reference_month=1, reference_year=2019)
        CallBalanceFactory(
            bill=bill,
            started_at=datetime(2019, 1, 10, 14, 0, 0))
        endpoint = self.get_endpoint(bill.subscriber)
        params = {
            'period': '01/2019',
        }
        response = self.client.get(endpoint, data=params, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        payload = response.json()
        self.assertEqual(payload['subscriber'], bill.subscriber)
        self.assertEqual(payload['reference_month'], bill.reference_month)
        self.assertEqual(payload['reference_year'], bill.reference_year)
        self.assertEqual(len(payload['calls']), 1)
