from datetime import datetime, timedelta, time
from decimal import Decimal

import factory

from billings.models import Bill, CallBalance, PriceRule


class BillFactory(factory.DjangoModelFactory):

    class Meta:
        model = Bill

    subscriber = '16997786564'
    reference_month = 1
    reference_year = 2019


class CallBalanceFactory(factory.DjangoModelFactory):

    class Meta:
        model = CallBalance

    bill = factory.SubFactory(BillFactory)
    call_id = '10'
    destination = '16997876654'
    started_at = datetime.utcnow()
    duration = timedelta(minutes=5)
    price = Decimal('0.50')


class PriceRuleFactory(factory.DjangoModelFactory):

    class Meta:
        model = PriceRule

    reduced_tariff_price = Decimal(0)
    standard_tariff_price = Decimal('0.09')
    standing_price = Decimal('0.36')
    reduced_tariff_start_time = time(hour=22)
    reduced_tariff_end_time = time(hour=6)
