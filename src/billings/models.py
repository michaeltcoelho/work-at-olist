import math
from datetime import timedelta
from decimal import Decimal

from django.db import models

from billings.utils import get_timedelta_from_time, time_in_range
from billings.querysets import PriceRuleQuerySet, BillQuerySet


class Bill(models.Model):

    subscriber = models.CharField('Subscriber', max_length=11)
    reference_month = models.IntegerField('Reference date')
    reference_year = models.IntegerField('Reference year')
    date_added = models.DateTimeField('Added date', auto_now_add=True)

    objects = BillQuerySet.as_manager()

    class Meta:
        verbose_name = 'Bill'
        verbose_name_plural = 'Bills'

    def __str__(self):
        return f'{self.reference_year}-{self.reference_month} - {self.subscriber}'


class CallBalance(models.Model):

    bill = models.ForeignKey(Bill, related_name='calls', on_delete=models.CASCADE)
    destination = models.CharField('Destination phone number', max_length=11)
    call_id = models.IntegerField('Call ID')
    started_at = models.DateTimeField('Call started at')
    duration = models.DurationField('Call duration')
    price = models.DecimalField('Price', decimal_places=2, max_digits=10)
    date_added = models.DateTimeField('Added', auto_now_add=True)

    class Meta:
        verbose_name = 'Call Balance'
        verbose_name_plural = 'Calls Balance'

    def __str__(self):
        return f'{self.bill_id} - {self.call_id} - {self.destination}'

    @property
    def call_duration(self):
        return self.duration.total_seconds() // 60


class PriceRule(models.Model):

    reduced_tariff_price = models.DecimalField('Reduced Tariff Price',
                                               decimal_places=2, max_digits=10)
    standard_tariff_price = models.DecimalField('Standard Tariff Price',
                                                decimal_places=2, max_digits=10)
    standing_price = models.DecimalField('Standing Price', decimal_places=2,
                                         max_digits=10)
    reduced_tariff_start_time = models.TimeField('Free tariff start time')
    reduced_tariff_end_time = models.TimeField('Free tariff end time')
    date_added = models.DateTimeField('Added date', auto_now_add=True)

    objects = PriceRuleQuerySet.as_manager()

    class Meta:
        verbose_name = 'Price rule'
        verbose_name_plural = 'Price rules'

    def __str__(self):
        return f'{self.id} - {self.date_added}'

    def is_in_reduced_tariff_time_range(self, call_time_at):
        return time_in_range(
            call_time_at.date(),
            self.reduced_tariff_start_time,
            self.reduced_tariff_end_time,
            call_time_at.time()
        )

    def _is_call_period_in_reduced_tariff_time(self, call_started_at, call_ended_at):
        return self.is_in_reduced_tariff_time_range(call_started_at)\
            and self.is_in_reduced_tariff_time_range(call_ended_at)

    def _get_reduced_tariff_duration(self, call_started_at, call_ended_at):
        call_started_time_at = get_timedelta_from_time(call_started_at.time())
        call_ended_time_at = get_timedelta_from_time(call_ended_at.time())
        reduced_tariff_start_time = get_timedelta_from_time(
            self.reduced_tariff_start_time
        )
        reduced_tariff_end_time = get_timedelta_from_time(self.reduced_tariff_end_time)

        reduced_tariff_time = timedelta(0)

        if self._is_call_period_in_reduced_tariff_time(call_started_at, call_ended_at):
            reduced_tariff_time = call_ended_time_at - call_started_time_at
        elif self.is_in_reduced_tariff_time_range(call_started_at):
            reduced_tariff_time = reduced_tariff_end_time - call_started_time_at
        elif self.is_in_reduced_tariff_time_range(call_ended_at):
            reduced_tariff_time = call_ended_time_at - reduced_tariff_start_time

        reduced_tariff_time_in_minutes = reduced_tariff_time.seconds / 60

        return reduced_tariff_time_in_minutes

    def get_reduced_and_standard_tariff_times(self, call_started_at, call_ended_at):
        call_duration = (call_ended_at - call_started_at).total_seconds() / 60
        reduced_tariff_duration = self._get_reduced_tariff_duration(
            call_started_at,
            call_ended_at)
        standard_tariff_duration = math.floor(call_duration - reduced_tariff_duration)
        return reduced_tariff_duration, standard_tariff_duration

    def _get_reduced_tariff_price(self, reduced_tariff_minutes):
        return self.reduced_tariff_price * Decimal(reduced_tariff_minutes)

    def _get_standard_tariff_price(self, standard_tariff_minutes):
        return self.standard_tariff_price * Decimal(standard_tariff_minutes)

    def get_tariff_price(self, reduced_tariff_minutes, standard_tariff_minutes):
        tariff_price = self._get_reduced_tariff_price(reduced_tariff_minutes)
        tariff_price += self._get_standard_tariff_price(standard_tariff_minutes)
        return self.standing_price + tariff_price

    def get_price(self, call_started_at, call_ended_at):
        tariff_minutes = self.get_reduced_and_standard_tariff_times(
            call_started_at,
            call_ended_at)
        reduced_tariff_minutes, standard_tariff_minutes = tariff_minutes
        call_price = self.get_tariff_price(
            reduced_tariff_minutes,
            standard_tariff_minutes)
        return call_price
