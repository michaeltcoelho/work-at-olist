import re
from collections import namedtuple
from datetime import date

from rest_framework import serializers


Period = namedtuple('Period', ['month', 'year'])


class PhoneField(serializers.CharField):

    def is_phone_number_valid(self, number):
        return re.match('^[0-9]{10,11}$', number)

    def to_internal_value(self, number):
        if not self.is_phone_number_valid(number):
            raise serializers.ValidationError('Invalid phone number.')
        return number


class PeriodField(serializers.CharField):

    def is_month_in_valid_range(self, month):
        return (1 <= month <= 12)

    def is_year_in_valid_range(self, year):
        current_year = date.today().year
        return (1900 <= year <= current_year)

    def is_period_valid(self, month, year):
        return not self.is_month_in_valid_range(month)\
            or not self.is_year_in_valid_range(year)

    def is_pattern_valid(self, value):
        return re.match(r'^(\d{2}/\d{4})$', value)

    def to_internal_value(self, value):
        if not self.is_pattern_valid(value):
            raise serializers.ValidationError(
                'Malformed period. It should be in month/year.')
        else:
            month, year = map(int, value.split('/'))
            if self.is_period_valid(month, year):
                raise serializers.ValidationError('Invalid period.')
            period = Period(month=month, year=year)
            return period
