"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.views.generic import RedirectView

from rest_framework_swagger.views import get_swagger_view


swagger_schema_view = get_swagger_view(title='Olist Challenge API')

urlpatterns = [
    path(r'', include('calls.urls')),
    path(r'', include('billings.urls')),
    path('', RedirectView.as_view(permanent=True, url='/docs/')),
    path(r'docs/', swagger_schema_view),
]
