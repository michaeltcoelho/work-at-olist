
.PHONY: test
test:
	@python src/manage.py test calls billings

.PHONY: loaddata
loaddata:
	@python src/manage.py loaddata price_rules.json
	@python src/manage.py loaddata records.json

.PHONY: migrate
migrate:
	@python src/manage.py migrate

.PHONY: install
install:
	@pip install -r requirements.txt
	@make migrate
	@make loaddata

.PHONY: start
start:
	@python src/manage.py runserver 0.0.0.0:8000
