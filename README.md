# Olist Challenge

This is an application proposed by Olist's tech team at https://github.com/olist/work-at-olist.

The implementation consists in a REST API that exposes two main resources.

- `/billings` - Provides an endpoint for getting monthly bills for a given phone number.
- `/calls` - Provides an endpoint for submiting call detail records.

### How this application has been implemented

You can submit call detail records of `start` or `end` types. Whenever a call is completed, that is, when the API receives both `start` and `end` records from the same `call_id` an event is triggered.

On the other side we have a handler in the `billings` module listening for events of completed calls type. Whenever it gets an event it gets the billing based on the call's month and year timestamp, following by the current active price rule, calculate the price for the call being handled and then create a new call balance.

### API Documentation

For submiting call detail records and get monthly bills through the REST API you can access the documentation at https://telecom-challenge.herokuapp.com/docs/

### Running locally

#### Requirements

- A virtualenv with Python 3.7+ installed. 
- SQLite3 installed on your machine.

#### Cloning

Clone this repo by running `git clone git@bitbucket.org:michaeltcoelho/work-at-olist.git` on your terminal.

Then `cd` to `/work-at-olist`.

#### Creating your local settings file

Create a `.env` file with the following content:

```bash
DEBUG=True
SECRET_KEY=mysecret
ALLOWED_HOSTS=0.0.0.0
DATABASE_URL=sqlite:///db.sqlite3
```

#### Installing the application

The command bellow will install the required dependencies for running the project
on your virtualenv.

Run `make install`

#### Running the applicaton server

Run `make start`. Access the application on your browser at `http://0.0.0.0:8000`

#### Running tests

You should change the settings in `.env` file to:

```bash
DEBUG=True
TESTING=True
SECRET_KEY=mysecret
ALLOWED_HOSTS=0.0.0.0
DATABASE_URL=sqlite://:memory:
```

Run `make test` on your terminal.

#### Work environment

- Visual Studio Code
- Macbook Pro 2015 / Mac OS Mojave
- Pyenv
- Pip
- Flake8
- Django, Django Rest Framework, Python Decouple
